﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.GraphQL.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.GraphQL.Schema
{
    public class CustomerMutation
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomerMutation(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public async Task<CreateCustomerPayload> CreateCustomer(CreateCustomer input)
        {
            var preferences = await _preferenceRepository
                            .GetRangeByIdsAsync(input.PreferenceIds.ToList());

            var customerId = Guid.NewGuid();
            var customer = new Customer()
            {
                Id = customerId,
                FirstName = input.FirstName,
                LastName = input.LastName,
                Email = input.Email,
                Preferences = preferences.Select(x => new CustomerPreference()
                {
                    CustomerId = customerId,
                    Preference = x,
                    PreferenceId = x.Id
                }).ToList()
            };

            await _customerRepository.AddAsync(customer);

            return new CreateCustomerPayload { Id = customer.Id };
        }

        public async Task<VoidType> EditCustomer(EditCustomer input)
        {
            var customer = await _customerRepository.GetByIdAsync(input.Id);

            if (customer == null)
                throw new NotFoundException();

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(input.PreferenceIds.ToList());

            customer.FirstName = input.FirstName;
            customer.LastName = input.LastName;
            customer.Email = input.Email;

            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();

            await _customerRepository.UpdateAsync(customer);

            return new VoidType();
        }

        public async Task<VoidType> DeleteCustomer(DeleteCustomer input)
        {
            var customer = await _customerRepository.GetByIdAsync(input.Id);

            if (customer == null)
                throw new NotFoundException();

            await _customerRepository.DeleteAsync(customer);

            return new VoidType();
        }
    }
}
