﻿using HotChocolate.Data;
using HotChocolate.Types;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.GraphQL.Models
{
    public class Queries
    {
        [UsePaging]
        [UseFiltering]
        [UseSorting]
        public IQueryable<Customer> GetCustomers(DataContext dbContext)
        => dbContext.Customers;
    }

}
