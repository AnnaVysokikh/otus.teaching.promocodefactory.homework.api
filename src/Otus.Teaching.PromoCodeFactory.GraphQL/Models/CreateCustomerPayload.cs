﻿using System;

namespace Otus.Teaching.PromoCodeFactory.GraphQL.Models
{
    public class CreateCustomerPayload
    {
        public Guid Id { get; set; }
    }
}
