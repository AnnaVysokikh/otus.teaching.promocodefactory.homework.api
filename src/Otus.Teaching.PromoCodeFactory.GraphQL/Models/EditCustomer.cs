﻿using System.Collections.Generic;
using System;

namespace Otus.Teaching.PromoCodeFactory.GraphQL.Models
{

    public class EditCustomer
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<Guid> PreferenceIds { get; set; }
    }
}
