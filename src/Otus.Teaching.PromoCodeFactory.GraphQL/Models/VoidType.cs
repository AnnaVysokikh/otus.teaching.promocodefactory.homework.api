﻿namespace Otus.Teaching.PromoCodeFactory.GraphQL.Models
{
    public class VoidType
    {
        public string Void { get; }
    }
}
