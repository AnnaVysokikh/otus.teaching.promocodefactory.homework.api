﻿using HotChocolate;

namespace Otus.Teaching.PromoCodeFactory.GraphQL
{
    public class GraphQLErrorFilter : IErrorFilter
    {
        public IError OnError(IError error)
        {
            if (error.Exception == null)
                return error;
            return error.WithMessage(error.Exception.Message);
        }
    }
}
