using Google.Protobuf.WellKnownTypes;
using Grpc;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using System.Linq;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Grps
{
    public class CustomersService : Customers.CustomersBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomerService(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }
        public override async Task<GetCustomersResponse> GetCustomers(Empty request, ServerCallContext context)
        {
            var allCustomers = await _customerRepository.GetAllAsync();

            var customers = allCustomers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id.ToString(),
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            var response = new GetCustomersResponse();
            response.Customers.AddRange(customers);

            return response;
        }

        public override async Task<CustomerResponse> GetCustomer(IdValue request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            var response = new CustomerResponse()
            {
                Id = customer.Id.ToString(),
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
            };
            response.Preferences.AddRange(customer.Preferences.Select(x => new PreferenceResponse()
            {
                Id = x.PreferenceId.ToString(),
                Name = x.Preference.Name
            }));

            return response;
        }

        public override async Task<IdValue> CreateCustomer(CreateCustomerRequest request, ServerCallContext context)
        {
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds.Select(Guid.Parse).ToList());

            var customerId = Guid.NewGuid();
            var customer = new Customer()
            {
                Id = customerId,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Preferences = preferences.Select(x => new CustomerPreference()
                {
                    CustomerId = customerId,
                    Preference = x,
                    PreferenceId = x.Id
                }).ToList()
            };

            await _customerRepository.AddAsync(customer);

            return new IdValue { Id = customer.Id.ToString() };
        }

        public override async Task<Empty> EditCustomer(EditCustomerRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            if (customer == null)
                throw new RpcException(new Status(StatusCode.NotFound, ""));

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds.Select(Guid.Parse).ToList());

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;

            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();

            await _customerRepository.UpdateAsync(customer);

            return new Empty();
        }

        public override async Task<Empty> DeleteCustomer(IdValue request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            if (customer == null)
                throw new RpcException(new Status(StatusCode.NotFound, ""));

            await _customerRepository.DeleteAsync(customer);

            return new Empty();
        }
    }
}
